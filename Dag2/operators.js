// === and !== operators
console.log(0 == false); // => true
console.log(0 === false); // => false

// &&, || and ! operators
console.log("abc" && 123); // => 123
console.log("abc" || 123); // => abc
console.log(!"abc"); // => false
console.log(!0); // => true

