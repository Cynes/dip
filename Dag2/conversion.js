// Falsy and truthy
var i = 2;
while (i--)
    console.log(i); // => 1 0

// Type conversion
console.log(true + 2); // => 3
console.log("3" + 4); // => 34
console.log("5" * 6); // => 30
console.log("x" - 7); // => NaN
console.log(!"ok" + 8); // => 8

