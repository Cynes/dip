// var statement
var x, y = 123;
console.log(x); // => undefined
console.log(y); // => 123
y = "test";
console.log(y); // => test
y = null;
console.log(y); // => null

// Hoisting
try {
    console.log(z); // => undefined
    var z = 456;
    console.log(z); // => 456
    console.log(u); // ReferenceError
}
catch (e) {
    console.log(e);
    // => ReferenceError: u is not defined
}

