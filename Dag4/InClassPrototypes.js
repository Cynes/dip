var d = Date.prototype;
Object.defineProperty(d, 'year',
    {
        get: function () {
            return this.getFullYear();
        },
        set: function (y) {
            this.setFullYear(y);
        }
    });

var now = new Date();
console.log(now.year);
now.year = 2016;
console.log(now.year);

