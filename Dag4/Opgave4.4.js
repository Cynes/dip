var list = [1,2,3,4,5,6,7,8,9,10];


var a = Array.prototype;
Object.defineProperty(a, 'sum',
    {
        value: function() {
            return this.reduce(function (sum, value) {
                    return sum + value;
                }, 0
            );
        }
    });
var b = Array.prototype;
Object.defineProperty(b, 'max',
    {
        value: function() {
                return Math.max.apply(null, this);
        }
    });

var c = Array.prototype;
Object.defineProperty(c, 'contains',
    {
        value: function(element) {
            if(this.indexOf(element) == -1){
                return false
            }
            return true;
        }
    });

console.log(list.sum());
console.log(list.max());
console.log(list.contains(1));
