var list = [5,2,8,10,7,6,9,3,1,4,11];
var stringList = ["string", "stri", "strin","st", "str", "s"];
var upperCaseMixedList = ["AndeRS", "Anders", "AnDerS", "WhEn YoU'Re TrYINg tO dO HoMEWorK"];

var compareMethod = function compare(s1, s2){
    if (s1 < s2){
        return  -1;
    }
    if (s1 == s2){
        return 0;
    }
    if (s1 > s2){
        return 1;
    }
}

var compareLenMethod = function compareLen(s1, s2){
    console.log(s1);
    console.log(s2);
    if (s1.length < s2.length){
        return  -1;
    }
    if (s1.length == s2.length){
        return 0;
    }
    if (s1.length > s2.length){
        return 1;
    }
}

var compareIgnoreCaseMethod =function compareIgnoreCase(s1, s2){
    if (s1.toLowerCase() < s2.toLowerCase()){
        return  -1;
    }
    if (s1.toLowerCase() == s2.toLowerCase()){
        return 0;
    }
    if (s1.toLowerCase()> s2.toLowerCase()){
        return 1;
    }
}

console.log(list.sort(compareMethod));
console.log(stringList.sort(compareLenMethod));
console.log(upperCaseMixedList.sort(compareIgnoreCaseMethod));