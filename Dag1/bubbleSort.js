var list = [5,3,2,8,1,21,10,32];
var list2 = [4,7,6,9,10,11,12,13];
var i, j, num, joinedList;

function bubbleSorter(i, j, list) {
    for (i = list.length - 1; i >= 0; i--) {
        for (j = 0; j <= i - 1; j++) {
           swap(i,j);
            }
        }
    }
}

function swap(i,j){
    if (list[j] > list[j + 1]) {
        var temp = list[j];
        list[j] = list[j + 1];
        list[j + 1] = temp;
}

function joinArrays(list, list2){
joinedList = list.concat(list2);
bubbleSorter(i,j,joinedList);
return joinedList;
}

function binarySearcher(num,list) {
    bubbleSorter(i, j, list);
    var startIndex  = 0,
        stopIndex   = list.length - 1,
        middle      = Math.floor((stopIndex + startIndex)/2);

    while(list[middle] != num && startIndex < stopIndex){

        if (num < list[middle]){
            stopIndex = middle - 1;
        } else if (num > list[middle]){
            startIndex = middle + 1;
        }
        middle = Math.floor((stopIndex + startIndex)/2);
    }

    return (list[middle] != num) ? -1 : middle;
}

function resetList(){
    list = [5,3,2,8,1,21,10,32];
}

function returnList(){
    return list;
}

joinArrays(list,list2);
console.log(binarySearcher(2,joinedList));
console.log(joinedList);


