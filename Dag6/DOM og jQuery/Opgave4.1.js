var personArray = [];
var personId = 1;

var Person = function(){

    return{
        name: "",
        email: "",
        phone: "",
        id: ""
    }
}

var Service = function() {
    return {
        createPerson: function (name, email, phone) {
            var person = new Person();
            person.name = name;
            person.email = email;
            person.phone = phone;
            person.id = personId;
            personId++;
            personArray.push(person);
            return person;
        }
        ,

        deletePerson: function (personId) {
            var removedPerson;
            for (var i = 0; i < personArray.length; i++) {
                if (personArray[i].id == personId) {
                    removedPerson = personArray[i].name + " - ID: " + personArray[i].id;
                    personArray.splice(i, 1);
                }
            }
            return removedPerson + " has been removed.";
        }
        ,

        updatePerson: function (pIdToFind, nameToUpdate, emailToUpdate, phoneToUpdate) {
            var personToUpdate;
            for (var i = 0; i < personArray.length; i++) {
                if (personArray[i].id == pIdToFind) {
                    personToUpdate = personArray[i].id;
                    personArray[i].name = nameToUpdate;
                    personArray[i].phone = phoneToUpdate;
                    personArray[i].email = emailToUpdate;
                }
            }
            return personToUpdate + " has been updated";
        }
        ,

        findPerson: function (pId) {
            for (var i = 0; i < personArray.length; i++) {
                if (personArray[i].id == pId) {
                    return personArray[i];
                }
            }
        }
    }

}



var service = new Service();
// var person = new Person();


//
//
// for(var i = 0; i<personArray.length; i++) {
//     console.log(
//         personArray[i].getName()
//         + personArray[i].getEmail()
//         + personArray[i].getPhone()
//         + personArray[i].getId()
//     );
//
// }
//





