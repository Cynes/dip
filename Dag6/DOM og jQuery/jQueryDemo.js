var newP = $("<p>").text("New paragraph");
$("h1").after(newP);

$("body").append("<p>Appended paragraph to body</p>");

$("p").first().after("<p>Second paragraph</p>");

$("p").css("backgroundColor", "lightgray");

$("p").each(function (i) {
    $(this).prepend("" + (i+1) + ": ");
});

