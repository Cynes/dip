var personArray = [];
var personId = 1;

var Person = function(){

    return{
        name: "",
        email: "",
        phone: "",
        id: ""
        }
}

var Service = function() {
    return {
        createPerson: function (name, email, phone) {
            var person = new Person();
            person.name = name;
            person.email = email;
            person.phone = phone;
            person.id = personId;
            personId++;
            personArray.push(person);
            return person;
        }
        ,

        deletePerson: function (personId) {
            var removedPerson;
            for (var i = 0; i < personArray.length; i++) {
                if (personArray[i].id == personId) {
                    removedPerson = personArray[i].name + " - ID: " + personArray[i].id;
                    personArray.splice(i, 1);
                }
            }
            return removedPerson + " has been removed.";
        }
        ,

        updatePerson: function (pIdToFind, nameToUpdate, emailToUpdate, phoneToUpdate) {
            var personToUpdate;
            for (var i = 0; i < personArray.length; i++) {
                if (personArray[i].id == pIdToFind) {
                    personToUpdate = personArray[i].getId();
                    personArray[i].name = nameToUpdate;
                    personArray[i].phone = phoneToUpdate;
                    personArray[i].email = emailToUpdate;
                }
            }
            return personToUpdate + " has been updated";
        }
        ,

        findPerson: function (pId) {
            for (var i = 0; i < personArray.length; i++) {
                if (personArray[i].id == pId) {
                    return personArray[i];
                }
            }
        }
    }

}



var service = new Service();

var p0 = service.createPerson("James T. Kirk", "JTK@Enterprise1701.ufp", "+45 84 34 12 43");
var p1 = service.createPerson("Hikaru Sulu", "FlamingSulu@Enterprise1701.ufp", "+46 32 12 12 43");
var p2 = service.createPerson("Montgomery Scott", "MS@Enterprise1701.ufp", "+45 43 12 24 65");
var p3 = service.createPerson("Redshirt #1", "spam@Enterprise1701.ufp", "+46 00 00 00 00");
var p4 = service.createPerson("Worf", "PruneJuiceRocks@Enterprise1701D.ufp", "+45 32 54 34 92");
var p5 = service.createPerson("Jean-Luc Picard", "TrueCaptain@Enterprise1701D.ufp", "+44 312 32 54 34");
var p6 = service.createPerson("Georgi Laforge", "WarpTheorist@Enterprise1701D.ufp", "+45 32 51 93 22");

function sortedNames(arr) {
        let nameArray = [];
        arr.forEach(function(person){
        nameArray.push(person.name);
    });
        return nameArray.sort().toString();
}

function nameByCountry(arr) {
       let namesByCountry = arr.filter(function (pers) {
           return pers.phone.substring(1, 3) == +45;
       }).map(function (pers){
           return pers.name + " - " + pers.phone;
    });
       return namesByCountry;
}


function addId(arr){
   let addedId = 0;
   let idAddedArray = [];
    arr.forEach(function(person){
        person.addedId = addedId;
        addedId++;
        idAddedArray.push(person);
    });
    return idAddedArray;
}


console.log(sortedNames(personArray));
console.log(nameByCountry(personArray));
console.log(addId(personArray));

