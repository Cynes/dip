function random(array){
    return function(){
        var i = Math.floor(Math.random() * array.length);
        console.log("index: " + i);
        return array[i];
    }
}

var die = [1,2,3,4,5,6];
var coin =["heads", "tails"];

var rollDie = random(die);
var flipCoin = random(coin);

console.log(rollDie());
console.log(flipCoin());