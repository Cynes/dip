function compare(s1, s2){
    if (s1 < s2){
        return  -1;
    }
    if (s1 === s2){
        return 0;
    }
    if (s1 > s2){
        return 1;
    }
}

function compareLen(s1, s2){
    if (s1.length < s2.length){
        return  -1;
    }
    if (s1.length === s2.length){
        return 0;
    }
    if (s1.length > s2.length){
        return 1;
    }
}

function compareIgnoreCase(s1, s2){
    if (s1.toLowerCase() < s2.toLowerCase()){
        return  -1;
    }
    if (s1.toLowerCase() === s2.toLowerCase()){
        return 0;
    }
    if (s1.toLowerCase()> s2.toLowerCase()){
        return 1;
    }
}

function compareSort(comp){
    return function(list){
            for (var i=0; i < list.length; i++) {
                var value = list[i];
                for (var j = i - 1; j >= 0 && comp(list[j],value)>0; j--) {
                    list[j+1] = list[j];
                }
                list[j+1] = value;
            }
            return list;
        };
    }

var lenSort = compareSort(compareLen);
var ignoreCaseSort = compareSort(compareIgnoreCase);

var list = ["anders", "Anders", "jeppe", "Jeppe", "MICHAEL", "michael", "MICHAEL"];
var list2 = ["aa", "aaaaa", "aaa", "aaaaaaaa", "aa", "a"];

console.log(lenSort(list2));
console.log(ignoreCaseSort(list));

