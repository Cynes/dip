var Subject = function () {


    return {
       observers: [],

        printObservers: function(){
           return this.observers;
        },

        registerObserver: function (o) {
            this.observers.push(o);
        },

        unregisterObserver: function (o) {
            var i = this.observers.indexOf(o)
            {
                if (i > -1) {
                    this.observers.splice(i, 1);
                }
            }
        },

        notifyObserver: function(o) {
            var i = this.observers.indexOf(o);
            if(i > -1) {
                this.observers[i].notify(i);
            }
        },

        notifyObservers: function () {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i].notify(i);
            }
        }
    }
};

var Observer = function () {
    return {
        notify: function (i) {
            console.log("observer " + i + " has been notified");
        }
    }
}



var subject = new Subject();

var o1 = new Observer();
var o2 = new Observer();
var o3 = new Observer();
var o4 = new Observer();
var o5 = new Observer();


subject.registerObserver(o1);
subject.registerObserver(o2);
subject.registerObserver(o3);
subject.registerObserver(o4);
subject.registerObserver(o5);

subject.notifyObserver(o3);
subject.notifyObservers();




