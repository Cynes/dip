function change(){
    var fade = function (node){
        var count = 0;
        var level = 1;
        var step = function(){
            var hex = level.toString(16);
            node.style.backgroundColor = '#FFFF' + hex + hex;

            while(count<15){
                level += 1;
                setTimeout(step, 100);
                count++;
            }
            while(count<30 && count>15){

                level -= 1;
                setTimeout(step, 100);
                count++;
            }
            count = 0;
        };
        setTimeout(step, 100);
    };
    fade(document.body);
}


function change2(){
    var fade = function (node){
        var level = 15;
        var step = function(){
            var hex = level.toString(16);
            node.style.backgroundColor = '#FFFF' + hex + hex;
            if (level<=15 && level >0){
                level -= 1;
                setTimeout(step, 100);
            }
        };
        setTimeout(step, 100);
    };
    fade(document.body);
}
