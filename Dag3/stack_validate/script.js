"use strict";

/**
 * Validates parenthesis balancing in the inputText element and writes a
 * message to the web page with the result.
 */
function validate() {
    var value = document.getElementById('inputText').value;

    var parentheses = "[]{}()",
        stack = [],
        i, character, bracePosition;

    for(i = 0; character = value[i]; i++) {
        bracePosition = parentheses.indexOf(character);
        console.log(bracePosition);

        if(bracePosition === -1) {
            continue;
        }
        if(bracePosition % 2 === 0) {
            stack.push(bracePosition + 1);
        }
        else {
            if(stack.pop() !== bracePosition) {
                return message("Not balanced!");
            }
        }
    }

    return (stack.length === 0) ? message("Everything is balanced!") : message("Not balanced!");

}

function message(text) {
    document.getElementById('output').innerHTML = text;
}