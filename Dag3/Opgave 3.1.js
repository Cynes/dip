var list = [5,2,8,10,7,6,9,3,1,4]
var i, j;

function bubbleSorter(i, j, list) {
    for (i = list.length - 1; i >= 0; i--) {
        for (j = 0; j <= i - 1; j++) {
            swap(i,j);
        }
    }
}
function swap(i,j) {
    if (list[j] > list[j + 1]) {
        var temp = list[j];
        list[j] = list[j + 1];
        list[j + 1] = temp;
    }
}

function binarySearcher(num,list) {
    var startIndex  = 0,
        stopIndex   = list.length - 1,
        middle      = Math.floor((stopIndex + startIndex)/2);

    while(list[middle] !== num && startIndex < stopIndex){

        if (num < list[middle]){
            stopIndex = middle - 1;
        } else if (num > list[middle]){
            startIndex = middle + 1;
        }
        middle = Math.floor((stopIndex + startIndex)/2);
    }

    return (list[middle] !== num) ? -1 : middle;
}
console.log(list);
bubbleSorter(i,j,list)
console.log(list);
console.log(binarySearcher(5,list));
