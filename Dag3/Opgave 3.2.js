var list = [5,2,8,10,7,6,9,3,1,4];
var stringList = ["a", "aaa", "aa", "aaaaa", "aaaa"];
var upperLowerCaseList = ["a", "b", "c", "d", "A", "B",  "C",  "D"];
var i, j;

function compare(s1, s2){
    if (s1 < s2){
        return  -1;
    }
    if (s1 === s2){
        return 0;
    }
    if (s1 > s2){
        return 1;
    }
}

function compareLen(s1, s2){
    if (s1.length < s2.length){
        return  -1;
    }
    if (s1.length === s2.length){
        return 0;
    }
    if (s1.length > s2.length){
        return 1;
    }
}

function compareIgnoreCase(s1, s2){
    if (s1.toLowerCase() < s2.toLowerCase()){
        return  -1;
    }
    if (s1.toLowerCase() === s2.toLowerCase()){
        return 0;
    }
    if (s1.toLowerCase()> s2.toLowerCase()){
        return 1;
    }
}

function bubbleSorter(i, j, listToSort, comp) {
    for (i = listToSort.length - 1; i >= 0; i--) {
        for (j = 0; j <= i - 1; j++) {
            swap(i, j, listToSort, comp);
        }
    }
}
function swap(i,j, listToSort,comp) {
    if (comp(listToSort[j],listToSort[j+1]) > 0) {
        var temp = listToSort[j];
        listToSort[j] = listToSort[j + 1];
        listToSort[j + 1] = temp;
    }
}
console.log(list);
bubbleSorter(i,j, list, compare);
console.log(list);

console.log(stringList);
bubbleSorter(i,j, stringList, compareLen);
console.log(stringList);

console.log(upperLowerCaseList);
bubbleSorter(i,j, upperLowerCaseList, compareIgnoreCase);
console.log(upperLowerCaseList);

/*
console.log(compare("a", "b"));
console.log(compareLen("aa", "b"));
console.log(compareIgnoreCase("Anders", "anders"));*/
