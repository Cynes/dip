var list = [1,2,3,4,5,6,7,8,9,10];


function max(array){
    return Math.max.apply(null, array);
}

function contains(array, element){
for(var i = 0; i<array.length; i++){
    if(array[i] === element){
        return true;
    }
}
return false;
}

function altContains(array, element){
    return array.indexOf(element);
}

console.log(altContains(list, 2));
function sum(array){
    return array.reduce(function(sum, value){
        return sum + value;}, 0
    );
}

function fib(n){
    if (n <= 1){
        return 1;
    }else {
        return fib(n - 1) + fib(n - 2);
    }
}

console.log(max(list));
console.log(contains(list, 3));
console.log(sum(list));
console.log(fib(6));