var list = [5,2,8,10,7,6,9,3,1,4]

var insertionSort = function(list) {

    for (var i=0; i < list.length; i++) {
        var value = list[i];
        for (var j = i - 1; j >= 0 && list[j] > value; j--) {
            list[j+1] = list[j];
        }
        list[j+1] = value;
    }
    return list;
};

console.log(insertionSort(list));