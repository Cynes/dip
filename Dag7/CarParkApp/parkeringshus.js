var urlData = 'http://www.odaa.dk/api/action/datastore_search?resource_id=2a82a145-0195-4081-a13c-b0e587e9b89c';
var carParkArray = new Array();

/*the $.getJSON() API call is asynchronous.
* using populateData will cause timing issues when trying to populate the
* HTML table.
* this can be solved by declaring a function as follows in the HTML script:
*
* setTimeout(function(){
*     >>>populate table code here<<<
*}, 1000)
*
* this will halt the creation of the table until the getJSON is finished fetching data.
*
* alternatively, use the populateDataSync function, which is declared right below populateData.

var populateData = function (url) {
    $.getJSON(url, function (carParks) {
            var recordsArray = carParks.result.records;
            for (i in recordsArray) {
                var carPark = {
                    vCnt: recordsArray[i].vehicleCount,
                    totSps: recordsArray[i].totalSpaces,
                    garCo: recordsArray[i].garageCode,
                    lastUpdate: recordsArray[i].date

                };
                carParkArray.push(carPark);
            }
        }
    );
};
*/

var populateDataSync = function(url){
$.ajax({
    async: false,
    url: url,
    dataType: "json",
    success: function(carParks) {
        var recordsArray = carParks.result.records;
        for (i in recordsArray) {
            var carPark = {
                vCnt: recordsArray[i].vehicleCount,
                totSps: recordsArray[i].totalSpaces,
                garCo: recordsArray[i].garageCode,
                lastUpdate: recordsArray[i].date

            };
            carParkArray.push(carPark);
        }
    },
});
}


var fetchData = function(){
    populateDataSync(urlData);
    return carParkArray;
}

// $.getJSON(url)
//     .done(function (users) {
//             for (var i in users)
//                 console.log(users[i].name);
//         }
//     )
//     .fail(function() {
//         console.log('Kan ikke hente JSON');
//     });

