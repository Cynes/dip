var urlData = 'https://randomuser.me/api/?results=20';
var personDataArray = new Array();

var populateDataSync = function(url){
$.ajax({
    async: false,
    url: url,
    dataType: "json",
    success: function(people) {
        var recordsArray = people.results;
        for (i in recordsArray) {
            var person = {
                name: recordsArray[i].name,
                pictureURL: recordsArray[i].picture.large,
            };
            personDataArray.push(person);
        }
    },
});
};


var fetchData = function(){
    populateDataSync(urlData);
    return personDataArray;
};
