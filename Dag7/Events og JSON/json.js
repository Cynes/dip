var json = '{"x":123,"a":[true,null,"test"]}';
var object = JSON.parse(json);
console.log(object.x); // => 123
json = JSON.stringify(object);
console.log(json); // => {"x":123,"a":[true,null,"test"]}

object = {x: 123, m: function(){}, a: [true, undefined, 'test']};
console.log(object.m); // => [Function]
console.log(object.a[1]); // => undefined
json = JSON.stringify(object,null,2);
console.log(json); // =>
// {
//   "x": 123,
//   "a": [
//     true,
//     null,
//     "test"
//   ]
// }

