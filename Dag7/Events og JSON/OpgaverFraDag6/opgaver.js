/* MED DOM

//opgave 1
var ps = document.querySelectorAll("*");
for (var i = 0; i < ps.length; i++) {
    ps[i].setAttribute("class", "red");
}

//opgave 2
var tableCss = document.querySelectorAll("tr");
for (var i = 0; i < tableCss.length; i++) {
    if (i % 2 == 0) {
        tableCss[i].setAttribute("class", "grey");
    }
}

//opgave 3
var prevSib = document.querySelectorAll("h1");
for (var i = 0; i < prevSib.length; i++) {
    var pToReplace = prevSib[i].nextElementSibling;
    var newH2 = document.createElement("h2");
    var h2Text = pToReplace.innerHTML;
    newH2.innerHTML = h2Text;
    prevSib[i].parentElement.insertBefore(newH2, prevSib[i].nextSibling);
    pToReplace.remove();
}*/

//opgave 1
$("*").attr("class", "red");

//opgave 2
$("tr:even").attr("class", "grey");

//opgave 3
$(document).ready(function () {
    $("h1").next().replaceWith(function () {
        return $("<h2 />", {html: $(this).html()});
    });

//opgave 4
    $("h1").each(function (i) {
        var name = "header" + i;
        $(this).attr('id', name)
    });

    $("h1").each(function (i) {
        var elem = $('<a>', {
            href: "#" + "header" + i,
            text: "header" + i
        });
        $("body").prepend("<br>");
        $("body").prepend(elem);
    });
//opgave 5
    $("table tr").each(function () {
        $("td:even").each(function (i) {
            $(this).text(personArray[i].name)
        })
        $("td:odd").each(function (i) {
            $(this).text(personArray[i].email)
        })
    })

});


var personArray = [];
var personId = 1;

var Person = function () {

    return {
        name: "",
        email: "",
        phone: "",
        id: ""
    }
}

var Service = function () {
    return {
        createPerson: function (name, email, phone) {
            var person = new Person();
            person.name = name;
            person.email = email;
            person.phone = phone;
            person.id = personId;
            personId++;
            personArray.push(person);
            return person;
        }
        ,

        deletePerson: function (personId) {
            var removedPerson;
            for (var i = 0; i < personArray.length; i++) {
                if (personArray[i].id == personId) {
                    removedPerson = personArray[i].name + " - ID: " + personArray[i].id;
                    personArray.splice(i, 1);
                }
            }
            return removedPerson + " has been removed.";
        }
        ,

        updatePerson: function (pIdToFind, nameToUpdate, emailToUpdate, phoneToUpdate) {
            var personToUpdate;
            for (var i = 0; i < personArray.length; i++) {
                if (personArray[i].id == pIdToFind) {
                    personToUpdate = personArray[i].id;
                    personArray[i].name = nameToUpdate;
                    personArray[i].phone = phoneToUpdate;
                    personArray[i].email = emailToUpdate;
                }
            }
            return personToUpdate + " has been updated";
        }
        ,

        findPerson: function (pId) {
            for (var i = 0; i < personArray.length; i++) {
                if (personArray[i].id == pId) {
                    return personArray[i];
                }
            }
        }
    }

}


var service = new Service();
// var person = new Person();

service.createPerson("James\n", "test@test\n", "23232313\n");
service.createPerson("James1\n", "test1@test\n", "123232313\n");
service.createPerson("James2\n", "test2@test\n", "223232313\n");
service.createPerson("James3\n", "test3@test\n", "323232313\n");
//
//
// for(var i = 0; i<personArray.length; i++) {
//     console.log(
//         personArray[i].getName()
//         + personArray[i].getEmail()
//         + personArray[i].getPhone()
//         + personArray[i].getId()
//     );
//
// }
//









